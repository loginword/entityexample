﻿using System;
using System.Collections.Generic;

namespace EntityExample.Models
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime OrderDate { get; set; }
        public virtual ICollection<Position> Positions { get; set; }
    }
}
