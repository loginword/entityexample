﻿using System.Collections.Generic;

namespace EntityExample.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Cost { get; set; }
        public bool IsAvailable { get; set; }
        public virtual ICollection<Position> Positions { get; set; }
    }
}
