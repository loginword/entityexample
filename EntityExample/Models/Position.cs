﻿using System.Collections.Generic;

namespace EntityExample.Models
{
    public class Position
    {
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int Count { get; set; }
    }
}
