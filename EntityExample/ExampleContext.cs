using System.ComponentModel.DataAnnotations.Schema;
using EntityExample.Models;

namespace EntityExample
{
    using System.Data.Entity;

    public class ExampleContext : DbContext
    {
        public ExampleContext()
            : base("name=ExampleContext")
        {
            Database.SetInitializer<ExampleContext>(new DropCreateDatabaseAlways<ExampleContext>());
        }

        public virtual DbSet<Position> Positions { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //���������� ��������� ������
            modelBuilder.Entity<Product>()
                .HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Order>()
                .HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Position>()
                .HasKey(x => new {x.OrderId, x.ProductId});

            //�������� ������ ����� ����������
            modelBuilder.Entity<Position>()
                .HasRequired(x => x.Order)
                .WithMany(x => x.Positions)
                .HasForeignKey(x => x.OrderId);
            modelBuilder.Entity<Position>()
                .HasRequired(x => x.Product)
                .WithMany(x => x.Positions)
                .HasForeignKey(x => x.ProductId);
        }
    }
}