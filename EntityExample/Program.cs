﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityExample.Models;

namespace EntityExample
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new ExampleContext())
            {
                var products = new List<Product>
                {
                    new Product {Title = "Milk", Cost = 100, IsAvailable = true},
                    new Product {Title = "Apple Juce", Cost = 120, IsAvailable = false},
                    new Product {Title = "Still Water", Cost=50, IsAvailable = true}
                };

                context.Products.AddRange(products);
                var result = context.SaveChanges();
                Console.WriteLine($"Added {result} products.");
                Console.WriteLine();

                var order = new Order
                {
                    OrderDate = DateTime.UtcNow,
                    Positions = new List<Position>
                {
                    new Position
                    {
                        Product = products[0],
                        Count = 3
                    },
                    new Position()
                    {
                        Product = products[2],
                        Count = 10
                    }
                }
                };
                context.Orders.Add(order);
                context.SaveChanges();
                Console.WriteLine($"Added order with Id = {order.Id}");
                Console.WriteLine();

                var orderFromDb = context.Orders.Find(order.Id);
                Console.WriteLine($"Order - {orderFromDb.OrderDate}:");
                foreach (var p in orderFromDb.Positions)
                {
                    Console.WriteLine($"{{Title: \"{p.Product.Title}\", Count: {p.Count}}}");
                }
            }
            Console.WriteLine("Press any key to stop...");
            Console.ReadKey();
        }
    }
}
